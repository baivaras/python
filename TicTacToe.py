from IPython.display import clear_output
import random 

# Method to draw the board
def display_board(board):
    clear_output()
    print(board[1] + '|' + board[2] + '|' + board[3])
    print(board[4] + '|' + board[5] + '|' + board[6])
    print(board[7] + '|' + board[8] + '|' + board[9])
    
#test_board = ['#','','O','X','O','X','O','X','O','X']
#draw_board(test_board)

# Method to choose player marker
def player_marker():
    marker = ''

    while marker != 'X' and marker != 'O':
        marker = input('Choose a marker. X or O: ')

    player1 = marker

    if player1 == 'X':
        player2 = 'O'
    else:
        player2 = 'X'

    return(player1,player2) # RETURNS TULIP

# Assing tulip values to seperate variables
#player1_marker, player2_marker = player_marker()
#print(player2_marker)

# Method to place a marker to chosen position
def place_marker(board, marker, position):
    board[position] = marker
    
#place_marker(test_board,'%',8)
#display_board(test_board)

# Method to check winner. Returns True or False for the marker.
def win_check(board, marker):
    return ((board[1] == marker and board[2] == marker and board[3] == marker) or 
           (board[4] == marker and board[5] == marker and board[6] == marker) or
           (board[7] == marker and board[8] == marker and board[9] == marker) or
           (board[1] == marker and board[5] == marker and board[9] == marker) or
           (board[3] == marker and board[5] == marker and board[7] == marker) or
           (board[1] == marker and board[4] == marker and board[7] == marker) or
           (board[2] == marker and board[5] == marker and board[8] == marker) or
           (board[3] == marker and board[6] == marker and board[9] == marker))

#win_check(test_board,'X')

# Method to choose random first player
def choose_first():
    return random.randint(1,2)
    #print(random_number)

#choose_random()

# Method to check if space on the board is free
def space_check(board, position):
        if board[position] == '':
            return True
        else: 
            return False

#space_check(test_board,1)
            
# Method to check if board is full or not.
def full_board_check(board):
    for item in range(0,10):
        if space_check(board, item):
            return False
    return True
            
#full_board_check(test_board)

# Method to return player choice.
def player_choice(board):
    choice = 0
    while choice not in [1,2,3,4,5,6,7,8,9] or not space_check(board,choice):
        choice = int(input('Please Choose Place 1-9 to Place Your Marker: '))
    
    return choice
    
#player_choice(test_board)

# Method to ask if players wants to play again.
def replay():
    play_again = ''
    
    while play_again != 'y' and play_again != 'n':
        play_again = input('Would you like to play again? Y or N: ').lower()
    
    if play_again == 'y':
        return True
    else:
        return False
    
#replay()

print('Welcome to Tic Tac Toe game!')
#print(player)
#

# Main logic for the game
while True:
    # Game setup
    turn = choose_first()
    print("Player {} will begin the game!".format(turn))
    player1_marker, player2_marker = player_marker()
    begin = input('Would you like to start the game? Y or N: ').lower()
    board = ['#','','','','','','','','','']
    
    if begin == 'y':
        game_on = True
    else: 
        game_on = False
    # Game start    
    while game_on:
        #Player 1 turn
        if turn == 1:
            display_board(board)
            choice = player_choice(board)
            place_marker(board,player1_marker,choice)
            
            if win_check(board,player1_marker):
                display_board(board)
                print('Congratulations, you won the game!')
                game_on = False
            else:
                if full_board_check(board):
                    display_board(board)
                    print('It is draw!')
                    game_on = False
                else: 
                    turn = 2
        else:
            # Player 2 turn 
            display_board(board)
            choice = player_choice(board)
            place_marker(board,player2_marker,choice)
            
            if win_check(board,player2_marker):
                display_board(board)
                print('Player2 won the game!')
                game_on = False
            else:
                if full_board_check(board):
                    display_board(board)
                    print('It is draw!')
                    game_on = False
                else: 
                    turn = 1

    if not replay():
        break
    